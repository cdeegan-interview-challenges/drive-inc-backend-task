import { FindOptionsWhere, getRepository } from 'typeorm';
import { Vehicle } from '../entities/Vehicle';

export const getVehicleById = async (id: string) => {
  const reservationRepo = getRepository(Vehicle);
  return reservationRepo.findOne({
    where: { id }
  });
};

export const getVehicles = async (where: FindOptionsWhere<Vehicle> = {}, includeReservations = false) => {
  const reservationRepo = getRepository(Vehicle);
  return reservationRepo.find({
    where,
    relations: { reservations: includeReservations }
  });
};
