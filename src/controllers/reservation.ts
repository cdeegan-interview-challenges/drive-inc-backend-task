import { FindOptionsWhere, getRepository } from 'typeorm';
import { Reservation } from '../entities/Reservation';
import { Vehicle } from '../entities/Vehicle';
import { getVehicles } from './vehicle';

const MAX_RESERVATION_RANGE_DAYS = 14;

const VEHICLE_RESERVATION_DAY_NUMBER_MAP: { [key: string]: number } = {
  'sun': 0,
  'mon': 1,
  'tue': 2,
  'wed': 3,
  'thur': 4,
  'fri': 5,
  'sat': 6
};

export const getReservationById = async (id: number) => {
  const reservationRepo = getRepository(Reservation);
  return reservationRepo.findOne({
    where: { id }
  });
};

export const getReservations = async (where: FindOptionsWhere<Reservation> = {}) => {
  const reservationRepo = getRepository(Reservation);
  return reservationRepo.find({ where });
};

export const createReservation = async (reservation: Reservation): Promise<Reservation> => {
  const reservationRepo = getRepository(Reservation);
  return await reservationRepo.save(reservation);
};

/**
 * Check if a Date object falls within a start and end time, where the start and end times are in the string format '12:00:00'.
 * Can optionally include an end time buffer, so that test drives are not booked too near close-of-business time
 */
export const isReservationTimeWithinRange = (startDate: Date, endDate: Date, startTimeRange: string, endTimeRange: string, endTimeBufferMins: number = 0) => {
  // create date objects for start + end, only time will be used so actual date not important
  const dummyStartDate = new Date('January 1, 2000 ' + startTimeRange);
  const dummyEndDate = new Date('January 1, 2000 ' + endTimeRange);
  const targetStartTimeS = startDate.getUTCSeconds() + (startDate.getUTCMinutes() * 60) + (startDate.getUTCHours() * 60 * 60);
  const targetEndTimeS = endDate.getUTCSeconds() + (endDate.getUTCMinutes() * 60) + (endDate.getUTCHours() * 60 * 60);
  const startTimeS = dummyStartDate.getUTCSeconds() + (dummyStartDate.getUTCMinutes() * 60) + (dummyStartDate.getUTCHours() * 60 * 60);
  const endTimeS = dummyEndDate.getUTCSeconds() + (dummyEndDate.getUTCMinutes() * 60) + (dummyEndDate.getUTCHours() * 60 * 60);
  return startTimeS <= targetStartTimeS && targetEndTimeS <= (endTimeS - (endTimeBufferMins * 60));
};

export const isReservationDayWithinRange = (startDate: Date, currentDate: Date = new Date()) => {
  const timeDiffMs = startDate.getTime() - currentDate.getTime();
  const timeDiffDays = timeDiffMs / (1000 * 3600 * 24);

  return Math.abs(timeDiffDays) <= MAX_RESERVATION_RANGE_DAYS;
};

export const listEligibleVehicles = async (vehicleType: string, location: string, startDate: Date, endDate: Date): Promise<Vehicle[]> => {
  if (!isReservationDayWithinRange(startDate)) { return []; }

  const eligbleVehicles = await getVehicles({ type: vehicleType, location }, true);

  // NOTE: ideally, the data would be completely filtered at the query level above

  // filter the matching vehicles to only those which can be reserved for that day/time
  return eligbleVehicles.filter((v) => {
    const isReservableThisDay = v.availableDays
      .map((dateAsString) => VEHICLE_RESERVATION_DAY_NUMBER_MAP[dateAsString])
      .some((dateAsInt) => dateAsInt === startDate.getDay());
    const isReservableThisTime = isReservationTimeWithinRange(startDate, endDate, v.availableFromTime, v.availableToTime, v.minimumMinutesBetweenBookings);
    return isReservableThisDay && isReservableThisTime;
  });
};

export const filterVehicleByAvailability = (vehicle: Vehicle, startDate: Date, endDate: Date) => {
  return vehicle.reservations.every((reservation) => {
    // this is more technically correct, but gives a worse user experience since the reservation would have to be at least 1 sec outside conflict
    // e.g. existing reservation ends at 12:30:00, free to create from 12:30:01 onwards  (ignoring minimumMinutesBetweenBookings)
    // better experience to allow booking from 12:30:00 onwards and let minimumMinutesBetweenBookings handle turnaround time
    // this way, the user does not need to worry about seconds when booking their reservation
    // return !(startDate.getTime() <= (reservation.endDateTime.getTime() + vehicle.minimumMinutesBetweenBookings * 60 * 1000)
    //   && endDate.getTime() >= (reservation.startDateTime.getTime() - vehicle.minimumMinutesBetweenBookings * 60 * 1000));

    // exclude if the requested reservation range conflicts with an existing reservation, including the vehicle's turnaroun time buffer
    return !(startDate.getTime() < (reservation.endDateTime.getTime() + vehicle.minimumMinutesBetweenBookings * 60 * 1000)
      && endDate.getTime() > (reservation.startDateTime.getTime() - vehicle.minimumMinutesBetweenBookings * 60 * 1000));
  });
};

export const findVehicleLeastReserved = (vehicles: Vehicle[]) => {
  return vehicles.reduce((prev, curr) => {
      return prev.reservations.length < curr.reservations.length ? prev : curr;
  });
};
