import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryColumn, OneToMany } from 'typeorm';
import { Reservation } from './Reservation';

@Entity()
export class Vehicle {
    @PrimaryColumn()
    id: string;

    @Column()
    type: string;

    @Column()
    location: string;

    @Column()
    availableFromTime: string;

    @Column()
    availableToTime: string;

    @Column({ type: 'simple-array', array: true })
    availableDays: string[];

    @Column()
    minimumMinutesBetweenBookings: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => Reservation, (reservation: Reservation) => reservation.vehicle)
    reservations: Reservation[]
}