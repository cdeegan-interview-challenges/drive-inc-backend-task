import * as express from 'express';
import { body } from 'express-validator';
import { notFound } from '@hapi/boom';
import { validate } from '../../middleware/validator';
import { createReservation, filterVehicleByAvailability, findVehicleLeastReserved, getReservations, listEligibleVehicles } from '../../controllers/reservation';
import { getVehicleById } from '../../controllers/vehicle';
import { Reservation } from '../../entities/Reservation';


const router = express.Router();

router.get('/', async (req, res, next) => {
  const data = await getReservations();
  res.json({ data });
});

router.post('/request',
  validate([
    body('location').not().isEmpty(),
    body('vehicleType').not().isEmpty(),
    body('startDateTime').not().isEmpty().isISO8601(),
    body('duration').not().isEmpty().isNumeric()
  ]),
  async (req, res, next) => {
    const startDate = new Date(req.body.startDateTime);
    const endDate = new Date(startDate.getTime() + req.body.duration * 60000);
    
    const eligibleVehicles = await listEligibleVehicles(req.body.vehicleType, req.body.location, startDate, endDate);
    if (eligibleVehicles.length === 0) { return res.json({ success: false, message: 'No matching vehicles with this criteria' }); }
  
    const availableVehicles = eligibleVehicles.filter(v => filterVehicleByAvailability(v, startDate, endDate));
    if (availableVehicles.length === 0) { return res.json({ success: false, message: 'No free vehicles at this time' }); }

    const leastReservedVehicle = findVehicleLeastReserved(availableVehicles);
    delete leastReservedVehicle.reservations;
    return res.json(leastReservedVehicle);
  });

router.post('/confirm',
  validate([
    body('vehicleId').not().isEmpty(),
    body('startDateTime').not().isEmpty().isISO8601(),
    body('durationMins').not().isEmpty().isNumeric(),
    body('customerName').not().isEmpty(),
    body('customerPhone').not().isEmpty(),
    body('customerEmail').not().isEmpty().isEmail()
  ]),
  async (req, res, next) => {
    const requestedVehicle = await getVehicleById(req.body.vehicleId);
    if (!requestedVehicle) { return next(notFound()); }
    const startDate = new Date(req.body.startDateTime);
    const endDate = new Date(startDate.getTime() + req.body.durationMins * 60000);
    
    const eligibleVehicles = await listEligibleVehicles(requestedVehicle.type, req.body.location, startDate, endDate);
    if (!eligibleVehicles.some(v => v.id === req.body.vehicleId)) { return res.json({ success: false, message: 'Vehicle cannot be reserved at this time' }); }
  
    const availableVehicles = eligibleVehicles.filter(v => filterVehicleByAvailability(v, startDate, endDate));
    if (!availableVehicles.some(v => v.id === req.body.vehicleId)) { return res.json({ success: false, message: 'Vehicle is already reserved at this time' }); }

    const reservation = new Reservation();
    reservation.vehicle = requestedVehicle;
    reservation.startDateTime = startDate;
    reservation.endDateTime = endDate;
    reservation.customerName = req.body.customerName;
    reservation.customerPhone = req.body.customerPhone;
    reservation.customerEmail = req.body.customerEmail;

    const newReservation = await createReservation(reservation);
    delete newReservation.vehicle;
    return res.json(newReservation);
  });

export default router;