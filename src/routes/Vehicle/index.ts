import * as express from 'express';
import * as Boom from '@hapi/boom';
import { getVehicleById, getVehicles } from '../../controllers/vehicle';

const router = express.Router();

router.get('/', async (req, res, next) => {
  const data = await getVehicles();
  res.json({ data });
});

router.get('/:id', async (req, res, next) => {
  const { id } = req.params;
  const data = await getVehicleById(id);

  if (data) { return res.json(data); }
  return next(Boom.notFound());
});

export default router;