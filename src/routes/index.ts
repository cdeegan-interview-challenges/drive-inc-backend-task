import * as express from 'express';
import ReservationRouter from './Reservation';
import VehicleRouter from './Vehicle';

const router = express.Router();

router.get('/', (req, next) => { req.body = 'vehicle-reservation-api'; });
router.use('/reserve', ReservationRouter);
router.use('/vehicle', VehicleRouter);

export default router;