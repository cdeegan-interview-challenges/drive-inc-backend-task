import { NextFunction, Request, Response } from 'express';

export default (req: Request, res: Response, next: NextFunction) => {
  console.log('req.headers.host', req.headers.host);
  console.log('req.headers.origin', req.headers.origin);
  res.header('Access-Control-Allow-Origin', req.headers.host.indexOf('localhost') > -1 ? req.headers.origin as string : '');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
};