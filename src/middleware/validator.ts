import { badData } from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';
import { validationResult, ValidationChain } from 'express-validator';

export const validate = (validations: ValidationChain[]) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    for (let validation of validations) {
      const result = await validation.run(req);
      if (result.array().length) { break; }
    }

    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }

    return next(badData(JSON.stringify({ errors: errors.array() })));
  };
};