import { Boom } from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';

export default (err: Boom, req: Request, res: Response, next: NextFunction) => {
  if (err.isBoom) {
    return res.status(err.output.payload.statusCode).send(err.output.payload.message);
  }
  res.status(500).send('An error occurred');
};