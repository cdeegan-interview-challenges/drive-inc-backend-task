import { Connection, createConnection } from 'typeorm';

export class TestHelper {

  private static _instance: TestHelper;

  private constructor() { }

  public static get instance(): TestHelper {
    if (!this._instance) this._instance = new TestHelper();
    return this._instance;
  }

  private connection: Connection;

  async setupTestDB() {
    // for this demo app, just use the same sqlite file for testing
    this.connection = await createConnection({
      type: 'sqlite',
      database: 'db.test.sqlite3',
      synchronize: true,
      logging: false,
      entities: [
        'src/entities/**/*.ts'
      ],
    });
  }

  teardownTestDB() {
    this.connection.close();
  }

}