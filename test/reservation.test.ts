import { afterAll, describe, expect, test, beforeAll } from '@jest/globals';
import { findVehicleLeastReserved, isReservationTimeWithinRange, isReservationDayWithinRange, listEligibleVehicles, filterVehicleByAvailability } from '../src/controllers/reservation';
import { TestHelper } from './TestHelper';

describe('reservations module', () => {

  beforeAll(async () => {
    await TestHelper.instance.setupTestDB();
  });

  afterAll(() => {
    TestHelper.instance.teardownTestDB();
  });

  test('isReservationDayWithinRange fails for dates exceeding limit', () => {
    const currentDate = new Date('November 1, 2023 00:00:00');
    const reservationDate = new Date('November 15, 2023 00:00:01');
    expect(isReservationDayWithinRange(reservationDate, currentDate)).toBe(false);
  });

  test('isReservationDayWithinRange succeeds for dates within limit', () => {
    const currentDate = new Date('November 1, 2023 00:00:00');
    const reservationDate = new Date('November 13, 2023 23:59:59');
    expect(isReservationDayWithinRange(reservationDate, currentDate)).toBe(true);
  });

  test('isReservationDayWithinRange succeeds for dates matching limit', () => {
    const currentDate = new Date('November 1, 2023 00:00:00');
    const reservationDate = new Date('November 15, 2023 00:00:00');
    expect(isReservationDayWithinRange(reservationDate, currentDate)).toBe(true);
  });

  test('isReservationTimeWithinRange succeeds for end date within time range', () => {
    const sDate = new Date('November 10, 2023 12:00:00');
    const eDate = new Date('November 10, 2023 12:45:00');
    const startTimeString = '00:00:00';
    const endTimeString = '13:00:00';
    expect(isReservationTimeWithinRange(sDate, eDate, startTimeString, endTimeString)).toBe(true);
  });

  test('isReservationTimeWithinRange succeeds for end date at edge time range', () => {
    const sDate = new Date('November 10, 2023 11:15:00');
    const eDate = new Date('November 10, 2023 12:00:00');
    const startTimeString = '00:00:00';
    const endTimeString = '12:00:00';
    expect(isReservationTimeWithinRange(sDate, eDate, startTimeString, endTimeString)).toBe(true);
  });

  test('isReservationTimeWithinRange fails for end date outside time range', () => {
    const sDate = new Date('November 10, 2023 11:15:00');
    const eDate = new Date('November 10, 2023 12:00:00');
    const startTimeString = '00:00:00';
    const endTimeString = '11:59:59';
    expect(isReservationTimeWithinRange(sDate, eDate, startTimeString, endTimeString)).toBe(false);
  });

  test('isReservationTimeWithinRange fails for start date outside time range', () => {
    const sDate = new Date('November 10, 2023 11:15:00');
    const eDate = new Date('November 10, 2023 12:00:00');
    const startTimeString = '11:30:00';
    const endTimeString = '16:00:00';
    expect(isReservationTimeWithinRange(sDate, eDate, startTimeString, endTimeString)).toBe(false);
  });

  test('isReservationTimeWithinRange fails for end date within time range, outside buffer', () => {
    const sDate = new Date('November 10, 2023 11:15:00');
    const eDate = new Date('November 10, 2023 12:01:00');
    const startTimeString = '00:00:00';
    const endTimeString = '12:45:00';
    expect(isReservationTimeWithinRange(sDate, eDate, startTimeString, endTimeString, 45)).toBe(false);
  });

  test('listEligibleVehicles should filter by eligibility criteria', async () => {
    const vehicles = await listEligibleVehicles(
      'tesla_model3',
      'dublin',
      new Date('November 10, 2023 12:00:00'),
      new Date('November 10, 2023 12:45:00')
    );
    const matchingVehicles = vehicles.filter((v) => v.type === 'tesla_model3' && v.location === 'dublin');
    expect(vehicles.length).toEqual(matchingVehicles.length);
  });

  test('listEligibleVehicles should return empty array for non-matching criteria', async () => {
    const vehicles = await listEligibleVehicles(
      'tesla_model3',
      'dublin',
      new Date('November 10, 2023 23:00:00'),
      new Date('November 10, 2023 23:45:00')
    );
    expect(vehicles.length).toEqual(0);
  });

  test('listEligibleVehicles should filter by reservation availability, start upper boundary conflict', async () => {
    const startDate = new Date('October 18, 2023 12:29:59Z');
    const endDate = new Date('October 18, 2023 14:15:00Z');
    const eligibleVehicles = await listEligibleVehicles('tesla_model3', 'dublin', startDate, endDate);
    const availableVehicles = eligibleVehicles.filter(v => filterVehicleByAvailability(v, startDate, endDate));

    expect(availableVehicles.length).toEqual(0);
  });

  test('listEligibleVehicles should filter by reservation availability, boundary OK', async () => {
    const startDate = new Date('October 18, 2023 12:30:00Z');
    const endDate = new Date('October 18, 2023 14:15:00Z');
    const eligibleVehicles = await listEligibleVehicles('tesla_model3', 'dublin', startDate, endDate);
    const availableVehicles = eligibleVehicles.filter(v => filterVehicleByAvailability(v, startDate, endDate));

    expect(availableVehicles.length).toEqual(1);
  });

  test('findVehicleLeastReserved should return the least-often reserved eligible car', async () => {
    const startDate = new Date('October 18, 2023 12:30:00Z');
    const endDate = new Date('October 18, 2023 14:15:00Z');
    const eligibleVehicles = await listEligibleVehicles('tesla_model3', 'cork', startDate, endDate);
    const leastReservedVehicle = findVehicleLeastReserved(eligibleVehicles);

    expect(eligibleVehicles.length).toEqual(2);
    expect(leastReservedVehicle.id).toEqual('tesla_1004');
  });
});